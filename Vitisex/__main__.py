#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on  2020

@author: Cheng Zou,BRC Bioinformatics Facility,Cornell University,cz355@cornell.edu

"""

#######
# Types of input, fasta, fasta.gz, fastq, fastq.gz
#
######
import logging
import argparse
import os,sys
import screed
import re
import pkg_resources
#from string import maketrans 
import shlex
import shutil
import subprocess
import bisect
from subprocess import Popen, PIPE
import numpy as np
from matplotlib import pyplot as plt
from scipy.special import betaln
from scipy.stats import beta
from scipy.stats import binom_test
from multiprocessing import Pool


REF_FILE=pkg_resources.resource_filename('Vitisex', 'data/reference.fasta')
SDR_FA=pkg_resources.resource_filename('Vitisex', 'data/SDR.fa')
TARGET_VCF=pkg_resources.resource_filename('Vitisex', 'data/concat4.GT.st.vcf.gz')
SEX_VAR=pkg_resources.resource_filename('Vitisex', 'data/v4_assign.txt') 


def getArgs():
    global args
    #### Checking requirememt ##############

    examples = "python -m Vitisex -f read_list -t number_of _thread \n"
    parse = argparse.ArgumentParser(description='Predict flower sex', epilog=examples)
    parse.add_argument('-f', dest='f', type=str, required=True, help='name of your fasta input')
    parse.add_argument('-O', dest='OUTDIR', type=str, required=False, help='set the PATH of the output,default is the working dir')
    parse.add_argument('-p', dest='PLOT',action='store_true', required=False,help='Generate plot or not')
    parse.add_argument('-b', dest='BBMAP', required=False, type=str, help='PATH of the BBmap suite')
    parse.add_argument('-g', dest='GATK', required=False, type=str, help='PATH of the GATK4 suite')
    parse.add_argument('-v', dest='BCF', required=False, type=str, help='PATH of bcftools ')
    parse.add_argument('-T', dest='T', default=8, required=False, type=int, help='Number of Thread')

    if sys.version_info[0] < 3:
        raise Exception("This code requires Python 3.")


    args = parse.parse_args()

    print(args)
    ### order of parameters
    ### the program will search for the software in PATH. if user does not defines the path of the software, it will use the result of the search.


    BBMAP = shutil.which("BBduk.sh")
    if BBMAP is None:
        if args.BBMAP is None:
            print("BBduk is not in PATH, please set the PATH of BBmap suite by setting -b ")
            sys.exit()           
    elif args.BBMAP is None:
        args.BBMAP=os.path.dirname(BBMAP)

    GATK = shutil.which("gatk")
    if GATK is None:
        if args.GATK is None:
            print("BBduk is not in PATH, please set the PATH of GATK4 by setting -g ")
            sys.exit()           
    elif args.GATK is None:
        args.GATK=os.path.dirname(GATK)

    BCF = shutil.which("bcftools")
    if BCF is None:
        if args.BCF is None:
            print("bcftools is not in PATH, please set the PATH of bcftools by setting -v ")
            sys.exit()           
    elif args.BCF is None:
        args.BCF=os.path.dirname(BCF)

    if args.OUTDIR is None:
        args.OUTDIR = os.getcwd()
    return args


def SDR_reads(sample_name, flist, ksize=23,ref=SDR_FA):

    logging.info("Step 1: Filting the Reads by Kmer overlapping with SDR")
    with open(f"{args.OUTDIR}/{sample_name}.fq.gz", 'wb') as f:
        for file in flist:
            cmd_kmer_filter = f"{args.BBMAP}/bbduk.sh  -Xmx64g  t={args.T} in={file} outm=stdout.fq.gz ref={ref} k={ksize} hdist=1"
            c1 = Popen(shlex.split(cmd_kmer_filter), stdout=f, universal_newlines=True)
            output=c1.wait()
            logging.info(f"filtering {file}  is done: {output}")
    
    print(output)


def mapping(sample_name, inp,ref=REF_FILE):
    cmd_map=f"bwa mem -t {args.T} -R \'@RG\\tID:{sample_name}\\tSM:{sample_name}\\tPL:ILLUMINA\' {ref}  {inp}  | samtools sort -@ {args.T} -o {inp}.sorted.bam -"
    print(cmd_map)
    ps = subprocess.Popen(cmd_map,shell=True)
    output = ps.communicate()[0]
    print(output)
    cmd_index=f"samtools index {inp}.sorted.bam"
    ps = subprocess.Popen(cmd_index,shell=True)
    output = ps.communicate()[0]
    logging.info("Step 2: Reads mapping")
    logging.info(f"mapping {sample_name} done: {output}")

    print(output)

def calling(sample_name, inp,target=TARGET_VCF, ref=REF_FILE ,region="VvCabSauv08_v1_Haplotig000002F_049:100000-260000"):

    cmd=f"{args.GATK}/gatk  --java-options \"-Xmx64g\" HaplotypeCaller -R  {ref}    -I  {inp}     -O {inp}.vcf.gz   --alleles {target}   -L {region}"
    print(cmd)
    ps = subprocess.Popen(cmd,shell=True)
    output = ps.communicate()[0]
    logging.info("Step 3: Variance calling for SDR only")
    logging.info(f"mapping {sample_name} done: {output}")

    cmd2=f"{args.BCF}/bcftools  query -f \'%CHROM\\t%POS\\t%REF\\t%ALT\\t%DP[\\t%AD]\\n\'  {inp}.vcf.gz  > {inp}.vcf.txt"
    print(cmd2)
    ps = subprocess.Popen(cmd2,shell=True)
    output = ps.communicate()[0]
    
    print(output)


def test_each(sample_name, inp, target=SEX_VAR):
    pos_dic={}
    for line in open(target,"r"):
        L=line.strip().split("\t")
        if L[0] not in pos_dic:
            pos_dic[L[0]]={"m":[],"f":[]}
        if L[-1]=="m":
            pos_dic[L[0]]["m"].append(L[2])
        if L[-1]=="f":
            pos_dic[L[0]]["f"].append(L[2])
    B_out={}
    for line in open(inp,"r"):
        L=line.strip().split("\t")
        if L[1] in pos_dic:	
            alleles=[L[2]]
            alleles.extend(L[3].split(","))
            cdic={}
            counts=L[-1].split(",")
            for i in range(len(counts)):
                if counts[i]!=".":
                    cdic[alleles[i]]=counts[i]
               

	
            print(alleles)
            print(cdic)
            print(L[1])
            print("male",pos_dic[L[1]]["m"] )
            print("female",pos_dic[L[1]]["f"])

            missing=0
            try:
                m=[int(cdic[i]) for i in pos_dic[L[1]]["m"]]  
            except KeyError:
                print("error")
                missing=1
                pass
            try:
                f=[int(cdic[i]) for i in pos_dic[L[1]]["f"]]  
            except KeyError:
                print("error")
                missing=1
                pass
            
            if missing ==0 and sum(m)+sum(f)>0:
                print(f"position:{L[1]},{m},{f}")
            
                p1=binom_test(sum(m), n=(sum(m)+sum(f)), p=0.001, alternative='two-sided')
                p2=binom_test(sum(m), n=(sum(m)+sum(f)), p=0.50, alternative='two-sided')
                p3=binom_test(sum(m), n=(sum(m)+sum(f)), p=0.999, alternative='two-sided')
                alist=[p1,p2,p3]
                B_out[int(L[1])]=[sum(m),sum(f), max(alist) ,alist.index(max(alist))]
    print(B_out)
    
    return B_out

def beta_binom(prior, y):
    """
    Compute the marginal likelihood, analytically, for a beta-binomial model.

    prior : tuple
        tuple of alpha and beta parameter for the prior (beta distribution)
    y : array
        array with "1" and "0" corresponding to the success and fails respectively
    """
    alpha, beta = prior
    h = np.sum(y)
    n = len(y)
    p_y = np.exp(betaln(alpha + h, beta+n-h) - betaln(alpha, beta))
    return p_y

def BF(sample_name,dic):
    keys=list(dic.keys())
    keys.sort()
    ranges = [(238293,260000),(216130,238292),(125069,216131),(106619,125068)]
    

    oup=open(f"{args.OUTDIR}/{sample_name}.test.txt", "w")
    oup2=open(f"{args.OUTDIR}/{sample_name}.BF.txt", "w")
       
    n=0
    for (lower, upper) in ranges:
        start = bisect.bisect_right(keys, lower)
        end=bisect.bisect_right(keys,upper)
        tlist=[dic[keys[x]][3]  for x in range(start,end)]
        y=[]
        for i in tlist:
            if i==0:
                y.extend([0,0])
            elif i==1:
                y.extend([0,1])
            else:
                y.extend([1,1])
        print(y)
        if len(y)>0:
            priors = ((1, len(y)+1), (int(len(y)/2) +1,int(len(y)/2)+1 ))
            BF = (beta_binom(priors[1], y) / beta_binom(priors[0], y))
            print(round(BF))
            if n==0:
                for x in range(start,end):
                    oup.write("A\t%s\t%s\n" % (keys[x], "\t".join( [str(a) for a in  dic[keys[x]]])))
                oup2.write(f"{sample_name}\t{BF}\t" )
            if n==1:
                for x in range(start,end):
                    oup.write("B\t%s\t%s\n" % (keys[x], "\t".join( [str(a) for a in  dic[keys[x]]])))
                oup2.write(f"{BF}\t" )

            if n==2:
                for x in range(start,end):
                    oup.write("C\t%s\t%s\n" % (keys[x], "\t".join( [str(a) for a in  dic[keys[x]]]) ))
                oup2.write(f"{BF}\t" )
            if n==3:
                for x in range(start,end):
                    oup.write("D\t%s\t%s\n" % (keys[x], "\t".join( [str(a) for a in  dic[keys[x]]]) ))
                oup2.write(f"{BF}\n" )

        else:
            if n==0:
                oup2.write(f"{sample_name}\tNA\t" )
            if n==1:
                oup2.write(f"NA\t" ) 
            if n==2:
                oup2.write(f"NA\n" ) 
            if n==3:
                oup2.write(f"NA\n" ) 


        n+=1 
    oup.close()
    oup2.close()
    logging.info("Step 4: calculating BF for each region")
    logging.info(f"BF calculation of {sample_name} is done")

def run_one(sample_name,flist):
    SDR_reads(sample_name,flist)
    mapping(sample_name, f"{args.OUTDIR}/{sample_name}.fq.gz")
    calling(sample_name, f"{args.OUTDIR}/{sample_name}.fq.gz.sorted.bam")
    B_out=test_each(sample_name,f"{args.OUTDIR}/{sample_name}.fq.gz.sorted.bam.vcf.txt"  )
    BF(sample_name,B_out)
    if args.PLOT==True:
        from . import gtplot
        gtplot.plot_4region(f"{args.OUTDIR}/{sample_name}.test.txt")

def main():

    logging.basicConfig(filename=args.OUTDIR + '/run.log',level=logging.DEBUG)

    sdic={}
    with open(args.f) as file:
        for line in file:
            L=line.strip().split()
            sample_name=L[0]
            sdic[L[0]]=L[1:]
            
    with Pool(processes=5) as p:
        results = p.starmap(run_one, ((k, vals) for k, vals in sdic.items()))


def test_ampseq():
    flist = [
        "/home/vitisgen/raw/AmpSeq_2019/10844_1570_100279_HGWF7AFXY_vDNAfen083G11_A01_F2_003_AACCACTC_TGTAGACC_R1.fastq.gz",
        "/home/vitisgen/raw/AmpSeq_2019/10844_1570_100279_HGWF7AFXY_vDNAfen083G11_A01_F2_003_AACCACTC_TGTAGACC_R2.fastq.gz"]
    sample_name = "10844_1570_100279_HGWF7AFXY_vDNAfen083G11_A01_F2"
    #SDR_reads(sample_name, flist)
    #mapping(sample_name, f"{args.OUTDIR}/{sample_name}.fq")
    calling(sample_name, f"{args.OUTDIR}/{sample_name}.fq.gz.sorted.bam")
    B_out=test_each(sample_name,f"{args.OUTDIR}/{sample_name}.fq.gz.sorted.bam.vcf.txt")
    BF(sample_name,B_out)

def test_reseq():
    flist = ["/local/workdir/chengzou/00.raw_reads/illumina/2020Dario/VvMerl181.illumina.R2.fastq.gz"]
    sample_name = "VvMerl181"
    #SDR_reads(sample_name, flist)
    #mapping(sample_name, f"{args.OUTDIR}/{sample_name}.fq.gz")
    #calling(sample_name, f"{args.OUTDIR}/{sample_name}.fq.gz.sorted.bam")
    B_out=test_each(sample_name,f"{args.OUTDIR}/{sample_name}.fq.gz.sorted.bam.vcf.txt"  )
    BF(sample_name,B_out)

def test_reseq2():
    flist = ["/local/workdir/chengzou/00.raw_reads/illumina/2020Dario/VvSylDVIT3603.16.illumina.R1.fastq.gz"]
    sample_name = "VvSylDVIT360316"
    #SDR_reads(sample_name, flist)
    #mapping(sample_name, f"{args.OUTDIR}/{sample_name}.fq.gz")
    calling(sample_name, f"{args.OUTDIR}/{sample_name}.fq.gz.sorted.bam")
    test_each(sample_name,f"{args.OUTDIR}/{sample_name}.fq.gz.sorted.bam.vcf.txt"  )


if __name__ == "__main__":
    getArgs()
    main()
   


