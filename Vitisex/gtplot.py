import os
import sys
import matplotlib.pyplot as plt
import ruptures as rpt
import numpy as np
import bisect
from itertools import cycle
from ruptures.utils import pairwise

#COLOR_CYCLE = ["#4286f4", "#f44174"]
COLOR_CYCLE = ["#4286f4", "#6b5b95","#f44174","#feb236"]
fake_start=255000

def display(signal, pos,  true_chg_pts, computed_chg_pts=None, **kwargs):
    """
    Display a signal and the change points provided in alternating colors. If another set of change
    point is provided, they are displayed with dashed vertical dashed lines.
    The following matplotlib subplots options is set by default, but can be changed when calling `display`):
    - "figsize": (10, 2 * n_features),  # figure size
    Args:
        signal (array): signal array, shape (n_samples,) or (n_samples, n_features).
               position (list) : position list
        true_chg_pts (list): list of change point indexes.
        computed_chg_pts (list, optional): list of change point indexes.
        **kwargs : all additional keyword arguments are passed to the plt.subplots call.
    Returns:
        tuple: (figure, axarr) with a :class:`matplotlib.figure.Figure` object and an array of Axes objects.
    """
    if type(signal) != np.ndarray:
        # Try to get array from Pandas dataframe
        signal = signal.values

    if signal.ndim == 1:
        signal = signal.reshape(-1, 1)
    n_samples, n_features = signal.shape

    # let's set a sensible defaut size for the subplots
    matplotlib_options = {
        "figsize": (10, 2 * n_features),  # figure size
    }
    # add/update the options given by the user
    matplotlib_options.update(kwargs)

    # create plots
    fig, axarr = plt.subplots(n_features, sharex=True, **matplotlib_options)
    if n_features == 1:
        axarr = [axarr]

    for axe, sig in zip(axarr, signal.T):
        color_cycle = cycle(COLOR_CYCLE)
        # plot s
        #axe.plot(range(n_samples), sig)
        #axe.plot(pos, sig, drawstyle='steps-mid')
        axe.scatter(pos, sig)

       

        # color each (true) regime
        bkps = [0] + sorted(true_chg_pts)
        alpha = 0.2  # transparency of the colored background

        for (start, end), col in zip(pairwise(bkps), color_cycle):
            axe.axvspan(max(0, start - 0.5),
                        end - 0.5,
                        facecolor=col, alpha=alpha)
        #print(bkps)


        reg_x=[]
        reg_y=[]
        for (start, end) in pairwise(bkps):
            mdic={}
            #print(start, end)
            #print(max(0,(bisect.bisect_left(pos, start)-1)),(bisect.bisect_left(pos, end)-1))
            Y_true=sig[ max(0,(bisect.bisect_left(pos, start)-1)):(bisect.bisect_left(pos, end)-1)]
            mdic[0]=np.square(np.subtract(Y_true,np.repeat(0, len(Y_true)))).mean()            
            mdic[1]=np.square(np.subtract(Y_true,np.repeat(1, len(Y_true)))).mean()
            mdic[2]=np.square(np.subtract(Y_true,np.repeat(2, len(Y_true)))).mean()
            #print(mdic)
            s = [(k, mdic[k]) for k in sorted(mdic, key=mdic.get, reverse=False)]
            #print(s)
            reg_y.append(s[0][0])
            reg_y.append(s[0][0])

            reg_x.append(start)
            reg_x.append(end)
        #print(reg_x,reg_y)
        axe.plot( reg_x,  reg_y, drawstyle='steps-mid')

        """
        color = "k"  # color of the lines indicating the computed_chg_pts
        linewidth = 3  # linewidth of the lines indicating the computed_chg_pts
        linestyle = "--"  # linestyle of the lines indicating the computed_chg_pts
        # vertical lines to mark the computed_chg_pts
        if computed_chg_pts is not None:
            for bkp in computed_chg_pts:
                if bkp != 0 and bkp < n_samples:
                    axe.axvline(x=bkp - 0.5,
                                color=color,
                                linewidth=linewidth,
                                linestyle=linestyle)
        """

    fig.tight_layout()

    return fig, axarr


def plot_4region(inp):
    d = {}
    for line in open(inp, "r"):
        L = line.strip().split()

        d[fake_start - int(L[1])] = int(L[5])
    s = [(k, d[k]) for k in sorted(d,reverse=False)]
    pos = [x[0] for x in s]
    #print(s)

    signal= np.array([x[1] for x in s])
    algo = rpt.Pelt(model="rbf").fit(signal)
    result = algo.predict(pen=10)
    #print(result)

    real_brks=[ pos[bisect.bisect_left(pos, x)-1]  for x in [fake_start-234681, fake_start-216130, fake_start-124562,fake_start-100000] ]
    #print(real_brks)
    #base=os.path.basename(inp)
    """
    oup=open(base+".brks","w")
    oup.write("%s\n" % "\t".join([str(x) for x in result ]))
    oup.write("%s\n" % "\t".join([str(pos[x-1]) for x in result ]))
    oup.close()
    """
    # display
    #rpt.display(signal,real_brks, result)
    display(signal,pos,real_brks, result)
    print("The plot is saved at ", inp +".reg.pdf")
    plt.savefig(inp +".reg.pdf")


if __name__ =="__main__":
    inp=sys.argv[1]
    plot_4region(inp)