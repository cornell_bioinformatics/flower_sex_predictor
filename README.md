# flower sex predictor for the _Vitis_ genus

The source code is distributed at [bitbucket](https://bitbucket.org/cornell_bioinformatics/flower_sex_predictor). And the code is not applicable to _Vitis rotundifolia_.

The code is developed under Python 3. 

## Software used in this package:

BBMap suite: version 35.50
bcftools:  version 1.9, Using htslib 1.9 
BWA MEM: version 0.7.17-r1188
GATK4 : version 4.1.4.0

The code is tested on these versions. 

The program will search for them automatically. If failed, it will ask you to specify the path. 

## Package install

git clone https://bitbucket.org/cornell_bioinformatics/flower_sex_predictor.git

pip install -e  [Your local directory]/Vitisex



## Prediction command line

usage:  python -m Vitisex   [-h] -f F [-O OUTDIR] [-p] [-b BBMAP] [-g GATK] [-v BCF]
                   [-T T]

Predict flower sex

optional arguments:
  -h, --help                                 show this help message and exit
  -f F                                          name of your reads input, both single ended reads or paired end reads works
  -O OUTDIR                            set the PATH of the output,default is the working dir
  -p                                            Generate plot or not
  -b BBMAP                              PATH of the BBmap suite
  -g GATK                                  PATH of the GATK4 suite
  -v BCF                                     PATH of bcftools
  -T T                                          Number of Thread

The format of the your reads file table: 

Column 1:  Sample name

Column 2:  Reads file 1, could be fasta or fastq files.

Column 3:  Reads file 2 (optional) 



The result contains: 

| File Name                                 | File description                                        |
| ----------------------------------------- | ------------------------------------------------------- |
| [SAMPLE_NAME].fq.gz                       | Filtered reads that are overlapped with SDR             |
| [SAMPLE_NAME].fq.gz.sorted.bam            | Reads mapping to the Cabernet Sauvignon “f” haplotype   |
| [SAMPLE_NAME].fq.gz.sorted.bam.bai        | Index file of bam file                                  |
| [SAMPLE_NAME].fq.gz.sorted.bam.vcf.gz     | Variances calling for SDR region                        |
| [SAMPLE_NAME].fq.gz.sorted.bam.vcf.gz.tbi | Index file of vcf file                                  |
| [SAMPLE_NAME].fq.gz.sorted.bam.vcf.txt    | table format of the variances                           |
| [SAMPLE_NAME].test.txt                    | Genotype for each sex-linked sites                      |
| [SAMPLE_NAME].BF.txt                      | Bayes Factor calculation for A,B,C,D region             |
| [SAMPLE_NAME].est.txt.reg.pdf             | Plot genotype for the A,B,C,D region (when -p is given) |



### if you want to generate the plot for your grapevine as the same time:

add -p to the command.

For example:

python -m Vitisex -f  read_list  -O pkg_test  -b  /programs/bbmap/      -g  /programs/gatk4/  -p



### if you want to generate the plot later, you could run

python -m Vitisex.gtplot  [sample_name].test.txt



## how to interpret the result

Let BFa denote the BF for A region, BFc denote the BF for C region. To decide the phenotype for flower sex:

BFa <0 and  BFc<0 Female flower (f/f)

BFa >0 and  BFc>0  male flower (M/f, M/H)

BFa<0 and   BFc>0  H flower (H/f, H/H )



To decide the genotype of this region, the  numeric genotype is indicated in the plot



| Phynotype        | f    | M    | H    | H     | H    | H     | H*    | M*   | M*   | M*   |
| ---------------- | ---- | ---- | ---- | ----- | ---- | ----- | ----- | ---- | ---- | ---- |
| Genotype         | f/f  | M/f  | H1/f | H1/H1 | H2/f | H1/H2 | H2/H2 | M/M  | >    | M/H2 |
| Numeric genotype | 0000 | 1111 | 0011 | 0022  | 0110 | 0121  | 0220  | 2222 | 1122 | 1221 |



![image-20201104150714538](image-20201104150714538.png)









